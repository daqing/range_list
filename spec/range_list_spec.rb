require_relative '../lib/range_list'

RSpec.describe RangeList do
  describe "#add" do
    context "when adds one range" do
      it "adds to range list" do
        rl = described_class.new

        rl.add([1, 5])
        expect(rl.to_s).to eq("[1, 5)")
      end
    end

    context "when adds more disjoint ranges" do
      it "adds to range list" do
        rl = described_class.new

        rl.add([1, 5])
        rl.add([10, 20])

        expect(rl.to_s).to eq("[1, 5) [10, 20)")
      end
    end

    context "when adds ranges that can expand existing ranges" do
      it "shows expanded ranges" do
        rl = described_class.new

        rl.add([1, 5])
        rl.add([10, 20])
        rl.add([20, 21])

        expect(rl.to_s).to eq("[1, 5) [10, 21)")
      end
    end

    context "when adds ranges that don't expand existing ranges" do
      it "shows expanded ranges" do
        rl = described_class.new

        rl.add([1, 5])
        rl.add([10, 20])
        rl.add([20, 20])

        expect(rl.to_s).to eq("[1, 5) [10, 20)")
      end
    end
  end

  describe "#remove" do
    context "when removes empty range" do
      it "remains the same" do
        el = described_class.new

        init_base(el)

        el.remove([10, 10])
        expect(el.to_s).to eq("[1, 8) [10, 21)")
      end
    end

    context "when changes one range" do
      it "works" do
        rl = described_class.new

        init_base(rl)

        rl.remove([10, 11])
        expect(rl.to_s).to eq("[1, 8) [11, 21)")
      end
    end

    context "when changes multiple ranges" do
      it "works" do
        rl = described_class.new

        init_base(rl)

        rl.remove([15, 17])
        rl.remove([3, 19])

        expect(rl.to_s).to eq("[1, 3) [19, 21)")
      end
    end
  end

  # Add base range data
  def init_base(rl)
    rl.add([1, 5])
    rl.add([10, 20])
    rl.add([20, 20])
    rl.add([20, 21])
    rl.add([2, 4])
    rl.add([3, 8])
  end
end

