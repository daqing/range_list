require_relative '../lib/one_range'

RSpec.describe OneRange do
  describe "#expand_from" do
    context "when can expand" do
      it "works" do
        r = described_class.new(10, 20)

        r.expands_from(described_class.new(20, 21))

        expect(r.start).to eq(10)
        expect(r.finish).to eq(21)
      end
    end

    context "when can't expand" do
      it "remains the same" do
        r = described_class.new(10, 20)

        n = r.expands_from(described_class.new(20, 20))

        expect(r.start).to eq(10)
        expect(r.finish).to eq(20)
        expect(n).to eq(0)
      end

      it "returns zero when can't expand" do
        r = described_class.new(1, 5)
        n = r.expands_from(described_class.new(20, 20))

        expect(n).to eq(0)
      end
    end
  end

  describe "#can_expand?" do
    context "when contains" do
      it "returns false" do
        r = described_class.new(1, 5)
        expect(r.can_expand?(OneRange.new(10, 21))).to eq(false)
      end
    end

    context "when can be expanded to the right" do
      it "returns true" do
        r = described_class.new(1, 5)
        expect(r.can_expand?(OneRange.new(3, 7))).to eq(true)
      end
    end

    context "when can be expanded to the left" do
      it "returns true" do
        r = described_class.new(7, 15)
        expect(r.can_expand?(OneRange.new(3, 9))).to eq(true)
      end
    end
  end

  describe "#contains?" do
    context "when contains another range" do
      it "returns true" do
        r = OneRange.new(10, 20)
        expect(r.contains?(OneRange.new(20, 20))).to eq(true)
      end
    end
  end

  describe "#can_shrink?" do
    context "when contains" do
      it "returns true" do
        r = described_class.new(10, 21)

        expect(r.can_shrink?(described_class.new(15, 17))).to eq(true)
        expect(r.can_shrink?(described_class.new(3, 19))).to eq(true)
        expect(r.can_shrink?(described_class.new(12, 28))).to eq(true)
      end
    end
  end

  describe "#empty?" do
    it "is empty" do
      r = OneRange.new(10, 21)

      expect(r.empty?(OneRange.new(10, 10))).to eq(true)
      expect(r.empty?(OneRange.new(21, 21))).to eq(true)
    end
  end
end
