require_relative './one_range'

class RangeList
  def initialize
    @ranges = []
  end

  def add(range)
    input = OneRange.new(range[0], range[1])

    n = 0
    contains = []
    @ranges.each do |range|
      if range.contains?(input)
        contains << true
      end

      n += range.expands_from(input)
    end

    @ranges << input if n == 0 && contains.empty?
  end

  def remove(range)
    input = OneRange.new(range[0], range[1])

    result = @ranges.map do |range|
      range.shrink_from(input)
    end

    @ranges = result.flatten
  end

  def to_s
    @ranges.map(&:to_s).join(" ")
  end

  def print
    puts(to_s)
  end
end
