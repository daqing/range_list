class OneRange
  attr_reader :start, :finish

  def initialize(start, finish)
    @start = start
    @finish = finish
  end

  def expands_from(range)
    return 0 unless can_expand?(range)

    n = 0

    if range.finish > finish
      @finish = range.finish
      n += 1
    end

    if range.start < start
      @start = range.start
      n += 1
    end

    n
  end

  def can_expand?(range)
    return false if contains?(range)

    can_expand_right?(range) || can_expand_left?(range)
  end

  def can_expand_right?(range)
    range.start >= start &&
    range.start <= finish &&
    range.finish > finish
  end

  def can_expand_left?(range)
    range.finish >= start &&
    range.finish <= finish &&
    range.start < start
  end

  def shrink_from(range)
    return [] if overlap?(range)
    return [self] unless can_shrink?(range)

    result = []

    if range.start > start && range.start < finish
      result << OneRange.new(start, range.start)
    end

    if range.finish > start && range.finish < finish
      result << OneRange.new(range.finish, finish)
    end

    result
  end

  def contains?(range)
    start <= range.start && range.finish <= finish
  end

  def can_shrink?(range)
    return false if empty?(range)
    return true if contains?(range)

    can_shrink_left?(range) || can_shrink_right?(range)
  end

  def can_shrink_left?(range)
    range.start >= start && range.start <= finish
  end

  def can_shrink_right?(range)
    range.finish >= start && range.finish <= finish
  end

  def empty?(range)
    range.exact? && (range.start == start || range.finish == finish)
  end

  def overlap?(range)
    range.start < start && range.finish > finish
  end

  def exact?
    start == finish
  end

  def to_s
    "[#{start}, #{finish})"
  end
end

